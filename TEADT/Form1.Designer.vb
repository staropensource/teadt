﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.cta = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ctb = New System.Windows.Forms.TextBox()
        Me.ctc = New System.Windows.Forms.TextBox()
        Me.ctd = New System.Windows.Forms.TextBox()
        Me.cte = New System.Windows.Forms.TextBox()
        Me.ctf = New System.Windows.Forms.TextBox()
        Me.ctg = New System.Windows.Forms.TextBox()
        Me.cth = New System.Windows.Forms.TextBox()
        Me.cti = New System.Windows.Forms.TextBox()
        Me.ctj = New System.Windows.Forms.TextBox()
        Me.ctk = New System.Windows.Forms.TextBox()
        Me.ctl = New System.Windows.Forms.TextBox()
        Me.ctm = New System.Windows.Forms.TextBox()
        Me.ctn = New System.Windows.Forms.TextBox()
        Me.cto = New System.Windows.Forms.TextBox()
        Me.ctp = New System.Windows.Forms.TextBox()
        Me.ctq = New System.Windows.Forms.TextBox()
        Me.ctr = New System.Windows.Forms.TextBox()
        Me.cts = New System.Windows.Forms.TextBox()
        Me.ctt = New System.Windows.Forms.TextBox()
        Me.ctu = New System.Windows.Forms.TextBox()
        Me.ctv = New System.Windows.Forms.TextBox()
        Me.ctw = New System.Windows.Forms.TextBox()
        Me.ctx = New System.Windows.Forms.TextBox()
        Me.cty = New System.Windows.Forms.TextBox()
        Me.ctz = New System.Windows.Forms.TextBox()
        Me.decrypted_encrypted_message = New System.Windows.Forms.TextBox()
        Me.Encrypt = New System.Windows.Forms.Button()
        Me.Decrypt = New System.Windows.Forms.Button()
        Me.Info = New System.Windows.Forms.Button()
        Me.ctdot = New System.Windows.Forms.TextBox()
        Me.ctspace = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cta
        '
        Me.cta.BackColor = System.Drawing.Color.Silver
        Me.cta.Location = New System.Drawing.Point(38, 48)
        Me.cta.Name = "cta"
        Me.cta.Size = New System.Drawing.Size(22, 20)
        Me.cta.TabIndex = 27
        Me.cta.Text = "#"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 340)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'ctb
        '
        Me.ctb.BackColor = System.Drawing.Color.Silver
        Me.ctb.Location = New System.Drawing.Point(120, 43)
        Me.ctb.Name = "ctb"
        Me.ctb.Size = New System.Drawing.Size(22, 20)
        Me.ctb.TabIndex = 1
        Me.ctb.Text = "$"
        '
        'ctc
        '
        Me.ctc.BackColor = System.Drawing.Color.Silver
        Me.ctc.Location = New System.Drawing.Point(38, 69)
        Me.ctc.Name = "ctc"
        Me.ctc.Size = New System.Drawing.Size(22, 20)
        Me.ctc.TabIndex = 2
        Me.ctc.Text = "§"
        '
        'ctd
        '
        Me.ctd.BackColor = System.Drawing.Color.Silver
        Me.ctd.Location = New System.Drawing.Point(120, 64)
        Me.ctd.Name = "ctd"
        Me.ctd.Size = New System.Drawing.Size(22, 20)
        Me.ctd.TabIndex = 3
        Me.ctd.Text = "%"
        '
        'cte
        '
        Me.cte.BackColor = System.Drawing.Color.Silver
        Me.cte.Location = New System.Drawing.Point(38, 90)
        Me.cte.Name = "cte"
        Me.cte.Size = New System.Drawing.Size(22, 20)
        Me.cte.TabIndex = 4
        Me.cte.Tag = ""
        Me.cte.Text = "''"
        '
        'ctf
        '
        Me.ctf.BackColor = System.Drawing.Color.Silver
        Me.ctf.Location = New System.Drawing.Point(120, 85)
        Me.ctf.Name = "ctf"
        Me.ctf.Size = New System.Drawing.Size(22, 20)
        Me.ctf.TabIndex = 5
        Me.ctf.Text = "~"
        '
        'ctg
        '
        Me.ctg.BackColor = System.Drawing.Color.Silver
        Me.ctg.Location = New System.Drawing.Point(38, 111)
        Me.ctg.Name = "ctg"
        Me.ctg.Size = New System.Drawing.Size(22, 20)
        Me.ctg.TabIndex = 6
        Me.ctg.Text = "µ"
        '
        'cth
        '
        Me.cth.BackColor = System.Drawing.Color.Silver
        Me.cth.Location = New System.Drawing.Point(120, 106)
        Me.cth.Name = "cth"
        Me.cth.Size = New System.Drawing.Size(22, 20)
        Me.cth.TabIndex = 7
        Me.cth.Text = "™"
        '
        'cti
        '
        Me.cti.BackColor = System.Drawing.Color.Silver
        Me.cti.Location = New System.Drawing.Point(38, 132)
        Me.cti.Name = "cti"
        Me.cti.Size = New System.Drawing.Size(22, 20)
        Me.cti.TabIndex = 8
        Me.cti.Text = "\"
        '
        'ctj
        '
        Me.ctj.BackColor = System.Drawing.Color.Silver
        Me.ctj.Location = New System.Drawing.Point(120, 127)
        Me.ctj.Name = "ctj"
        Me.ctj.Size = New System.Drawing.Size(22, 20)
        Me.ctj.TabIndex = 9
        Me.ctj.Text = "ß"
        '
        'ctk
        '
        Me.ctk.BackColor = System.Drawing.Color.Silver
        Me.ctk.Location = New System.Drawing.Point(38, 153)
        Me.ctk.Name = "ctk"
        Me.ctk.Size = New System.Drawing.Size(22, 20)
        Me.ctk.TabIndex = 10
        Me.ctk.Text = "_"
        '
        'ctl
        '
        Me.ctl.BackColor = System.Drawing.Color.Silver
        Me.ctl.Location = New System.Drawing.Point(120, 148)
        Me.ctl.Name = "ctl"
        Me.ctl.Size = New System.Drawing.Size(22, 20)
        Me.ctl.TabIndex = 11
        Me.ctl.Text = "*"
        '
        'ctm
        '
        Me.ctm.BackColor = System.Drawing.Color.Silver
        Me.ctm.Location = New System.Drawing.Point(38, 174)
        Me.ctm.Name = "ctm"
        Me.ctm.Size = New System.Drawing.Size(22, 20)
        Me.ctm.TabIndex = 12
        Me.ctm.Text = "&"
        '
        'ctn
        '
        Me.ctn.BackColor = System.Drawing.Color.Silver
        Me.ctn.Location = New System.Drawing.Point(120, 169)
        Me.ctn.Name = "ctn"
        Me.ctn.Size = New System.Drawing.Size(22, 20)
        Me.ctn.TabIndex = 13
        Me.ctn.Text = "+"
        '
        'cto
        '
        Me.cto.BackColor = System.Drawing.Color.Silver
        Me.cto.Location = New System.Drawing.Point(38, 194)
        Me.cto.Name = "cto"
        Me.cto.Size = New System.Drawing.Size(22, 20)
        Me.cto.TabIndex = 14
        Me.cto.Text = "³"
        '
        'ctp
        '
        Me.ctp.BackColor = System.Drawing.Color.Silver
        Me.ctp.Location = New System.Drawing.Point(120, 190)
        Me.ctp.Name = "ctp"
        Me.ctp.Size = New System.Drawing.Size(22, 20)
        Me.ctp.TabIndex = 15
        Me.ctp.Text = "^"
        '
        'ctq
        '
        Me.ctq.BackColor = System.Drawing.Color.Silver
        Me.ctq.Location = New System.Drawing.Point(38, 215)
        Me.ctq.Name = "ctq"
        Me.ctq.Size = New System.Drawing.Size(22, 20)
        Me.ctq.TabIndex = 16
        Me.ctq.Text = "°"
        '
        'ctr
        '
        Me.ctr.BackColor = System.Drawing.Color.Silver
        Me.ctr.Location = New System.Drawing.Point(120, 211)
        Me.ctr.Name = "ctr"
        Me.ctr.Size = New System.Drawing.Size(22, 20)
        Me.ctr.TabIndex = 17
        Me.ctr.Text = "<"
        '
        'cts
        '
        Me.cts.BackColor = System.Drawing.Color.Silver
        Me.cts.Location = New System.Drawing.Point(38, 233)
        Me.cts.Name = "cts"
        Me.cts.Size = New System.Drawing.Size(22, 20)
        Me.cts.TabIndex = 18
        Me.cts.Text = ">"
        '
        'ctt
        '
        Me.ctt.BackColor = System.Drawing.Color.Silver
        Me.ctt.Location = New System.Drawing.Point(120, 232)
        Me.ctt.Name = "ctt"
        Me.ctt.Size = New System.Drawing.Size(22, 20)
        Me.ctt.TabIndex = 19
        Me.ctt.Text = "|"
        '
        'ctu
        '
        Me.ctu.BackColor = System.Drawing.Color.Silver
        Me.ctu.Location = New System.Drawing.Point(38, 253)
        Me.ctu.Name = "ctu"
        Me.ctu.Size = New System.Drawing.Size(22, 20)
        Me.ctu.TabIndex = 20
        Me.ctu.Text = "²"
        '
        'ctv
        '
        Me.ctv.BackColor = System.Drawing.Color.Silver
        Me.ctv.Location = New System.Drawing.Point(120, 253)
        Me.ctv.Name = "ctv"
        Me.ctv.Size = New System.Drawing.Size(22, 20)
        Me.ctv.TabIndex = 21
        Me.ctv.Text = "◙"
        '
        'ctw
        '
        Me.ctw.BackColor = System.Drawing.Color.Silver
        Me.ctw.Location = New System.Drawing.Point(38, 274)
        Me.ctw.Name = "ctw"
        Me.ctw.Size = New System.Drawing.Size(22, 20)
        Me.ctw.TabIndex = 22
        Me.ctw.Text = "`"
        '
        'ctx
        '
        Me.ctx.BackColor = System.Drawing.Color.Silver
        Me.ctx.Location = New System.Drawing.Point(120, 274)
        Me.ctx.Name = "ctx"
        Me.ctx.Size = New System.Drawing.Size(22, 20)
        Me.ctx.TabIndex = 23
        Me.ctx.Text = "}"
        '
        'cty
        '
        Me.cty.BackColor = System.Drawing.Color.Silver
        Me.cty.Location = New System.Drawing.Point(38, 294)
        Me.cty.Name = "cty"
        Me.cty.Size = New System.Drawing.Size(22, 20)
        Me.cty.TabIndex = 24
        Me.cty.Text = "´"
        '
        'ctz
        '
        Me.ctz.BackColor = System.Drawing.Color.Silver
        Me.ctz.Location = New System.Drawing.Point(120, 294)
        Me.ctz.Name = "ctz"
        Me.ctz.Size = New System.Drawing.Size(22, 20)
        Me.ctz.TabIndex = 25
        Me.ctz.Text = "{"
        '
        'decrypted_encrypted_message
        '
        Me.decrypted_encrypted_message.BackColor = System.Drawing.Color.Silver
        Me.decrypted_encrypted_message.Location = New System.Drawing.Point(223, 9)
        Me.decrypted_encrypted_message.Multiline = True
        Me.decrypted_encrypted_message.Name = "decrypted_encrypted_message"
        Me.decrypted_encrypted_message.Size = New System.Drawing.Size(699, 340)
        Me.decrypted_encrypted_message.TabIndex = 28
        '
        'Encrypt
        '
        Me.Encrypt.BackColor = System.Drawing.Color.Silver
        Me.Encrypt.Location = New System.Drawing.Point(928, 9)
        Me.Encrypt.Name = "Encrypt"
        Me.Encrypt.Size = New System.Drawing.Size(75, 23)
        Me.Encrypt.TabIndex = 29
        Me.Encrypt.Text = "Encrypt"
        Me.Encrypt.UseVisualStyleBackColor = False
        '
        'Decrypt
        '
        Me.Decrypt.BackColor = System.Drawing.Color.Silver
        Me.Decrypt.Location = New System.Drawing.Point(928, 330)
        Me.Decrypt.Name = "Decrypt"
        Me.Decrypt.Size = New System.Drawing.Size(75, 23)
        Me.Decrypt.TabIndex = 30
        Me.Decrypt.Text = "Decrypt"
        Me.Decrypt.UseVisualStyleBackColor = False
        '
        'Info
        '
        Me.Info.BackColor = System.Drawing.Color.Silver
        Me.Info.Location = New System.Drawing.Point(928, 160)
        Me.Info.Name = "Info"
        Me.Info.Size = New System.Drawing.Size(75, 23)
        Me.Info.TabIndex = 31
        Me.Info.Text = "Info"
        Me.Info.UseVisualStyleBackColor = False
        '
        'ctdot
        '
        Me.ctdot.BackColor = System.Drawing.Color.Silver
        Me.ctdot.Location = New System.Drawing.Point(54, 330)
        Me.ctdot.Name = "ctdot"
        Me.ctdot.Size = New System.Drawing.Size(22, 20)
        Me.ctdot.TabIndex = 32
        Me.ctdot.Text = "["
        '
        'ctspace
        '
        Me.ctspace.BackColor = System.Drawing.Color.Silver
        Me.ctspace.Location = New System.Drawing.Point(158, 329)
        Me.ctspace.Name = "ctspace"
        Me.ctspace.Size = New System.Drawing.Size(22, 20)
        Me.ctspace.TabIndex = 33
        Me.ctspace.Text = "]"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1006, 362)
        Me.Controls.Add(Me.ctspace)
        Me.Controls.Add(Me.ctdot)
        Me.Controls.Add(Me.Info)
        Me.Controls.Add(Me.Decrypt)
        Me.Controls.Add(Me.Encrypt)
        Me.Controls.Add(Me.decrypted_encrypted_message)
        Me.Controls.Add(Me.cta)
        Me.Controls.Add(Me.ctz)
        Me.Controls.Add(Me.cty)
        Me.Controls.Add(Me.ctx)
        Me.Controls.Add(Me.ctw)
        Me.Controls.Add(Me.ctv)
        Me.Controls.Add(Me.ctu)
        Me.Controls.Add(Me.ctt)
        Me.Controls.Add(Me.cts)
        Me.Controls.Add(Me.ctr)
        Me.Controls.Add(Me.ctq)
        Me.Controls.Add(Me.ctp)
        Me.Controls.Add(Me.cto)
        Me.Controls.Add(Me.ctn)
        Me.Controls.Add(Me.ctm)
        Me.Controls.Add(Me.ctl)
        Me.Controls.Add(Me.ctk)
        Me.Controls.Add(Me.ctj)
        Me.Controls.Add(Me.cti)
        Me.Controls.Add(Me.cth)
        Me.Controls.Add(Me.ctg)
        Me.Controls.Add(Me.ctf)
        Me.Controls.Add(Me.cte)
        Me.Controls.Add(Me.ctd)
        Me.Controls.Add(Me.ctc)
        Me.Controls.Add(Me.ctb)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TEADT"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ctb As System.Windows.Forms.TextBox
    Friend WithEvents ctc As System.Windows.Forms.TextBox
    Friend WithEvents ctd As System.Windows.Forms.TextBox
    Friend WithEvents cte As System.Windows.Forms.TextBox
    Friend WithEvents ctf As System.Windows.Forms.TextBox
    Friend WithEvents ctg As System.Windows.Forms.TextBox
    Friend WithEvents cth As System.Windows.Forms.TextBox
    Friend WithEvents cti As System.Windows.Forms.TextBox
    Friend WithEvents ctj As System.Windows.Forms.TextBox
    Friend WithEvents ctk As System.Windows.Forms.TextBox
    Friend WithEvents ctl As System.Windows.Forms.TextBox
    Friend WithEvents ctm As System.Windows.Forms.TextBox
    Friend WithEvents ctn As System.Windows.Forms.TextBox
    Friend WithEvents cto As System.Windows.Forms.TextBox
    Friend WithEvents ctp As System.Windows.Forms.TextBox
    Friend WithEvents ctq As System.Windows.Forms.TextBox
    Friend WithEvents ctr As System.Windows.Forms.TextBox
    Friend WithEvents cts As System.Windows.Forms.TextBox
    Friend WithEvents ctt As System.Windows.Forms.TextBox
    Friend WithEvents ctu As System.Windows.Forms.TextBox
    Friend WithEvents ctv As System.Windows.Forms.TextBox
    Friend WithEvents ctw As System.Windows.Forms.TextBox
    Friend WithEvents ctx As System.Windows.Forms.TextBox
    Friend WithEvents cty As System.Windows.Forms.TextBox
    Friend WithEvents ctz As System.Windows.Forms.TextBox
    Friend WithEvents cta As System.Windows.Forms.TextBox
    Friend WithEvents Encrypt As System.Windows.Forms.Button
    Friend WithEvents Decrypt As System.Windows.Forms.Button
    Friend WithEvents Info As System.Windows.Forms.Button
    Friend WithEvents ctdot As System.Windows.Forms.TextBox
    Friend WithEvents ctspace As System.Windows.Forms.TextBox
    Protected Friend WithEvents decrypted_encrypted_message As System.Windows.Forms.TextBox

End Class
