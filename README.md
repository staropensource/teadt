# What is TEADT?
TEADT is a tool to encrypt and decrypt messages securely. It uses a so called "crypt table" for the encryption and decryption process. If you give a person a encrypted TEADT message and you aren't using the default crypt table, the other person won't be able to decrypt it easily without your used crypt table.

# Recommended use
We recommend that you share your crypt table with the person you want to send your encrypted data once. We recommend using [Syncthing](https://syncthing.net), since its decentralized. After that, share your encrypted message to the person. We still recommend using Syncthing for that, but since the service (or the other users) wont't have the crypt table, you can still send the encrypted message there.

# Downloads
[You can find the current version of TEADT on it's itch.io page.](https://jeremystartm.itch.io/teadt)

# What does TEADT mean?
TEADT stands for "Text Encryption And Decryption Tool".

# What does TEADT require to run?
TEADT requires nothing but .NET Framework 4.8. You can get it [here](https://gitlab.com/staropensource/teadt/-/raw/master/DOTNET%20Framework%204.8%20Packages/DOTNET%20Framework%204.8%20Runtime.exe)

# Building
You need the following things installed in order to build TEADT (things that have ## at the end require Windows, you can use a VM if you have no Windows installed):
 * .NET Framework 4.8 Developer Pack & Runtime (get them [here](https://gitlab.com/staropensource/teadt/-/raw/master/DOTNET%20Framework%204.8%20Packages/DOTNET%20Framework%204.8%20Developer%20Pack.exe)) ##
 * Visual Basic 2010 Express ##
 * Git
Building TEADT:
 * Clone the repository with **https://gitlab.com/staropensource/teadt**
 * Open Visual Basic 2010 Express and select the TEADT.sln file to open the project.
 * Edit source.... or continue
 * When you are done with editing TEADT, press the play button (or press F5)
